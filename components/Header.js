import * as React from "react";
import { View } from "react-native";
import { Appbar } from "react-native-paper";

const Header = (props) => {
    const _handleMore = () => alert("Я Женя и я молодец!");
    return (
        <View>
            <Appbar.Header>
                <Appbar.Content title={props.title} />
                <Appbar.Action icon="dots-vertical" onPress={_handleMore} />
            </Appbar.Header>
        </View>
    );
};

export default Header;
