import React, { Component } from "react";
import { View } from "react-native";
import { Tabs, TabScreen } from "react-native-paper-tabs";
import CostsPage from "./Pages/CostsPage";
import CostsPageTest from "./Pages/CostsPageTest";

import moment from 'moment';

import getDataInStorage from '../src/getDataFromStorage';
import setDataInStorage, {clearStorage} from '../src/setDataInStorage';

export default class TabsCosts extends Component {

    constructor(props) {
        super(props);

        this.storageCostKey = 'costs';
        this.storageCategoryKey = 'category';

        this.state = {chosenDate: new Date()};
    }

    async componentDidMount() {  
        let costs = await getDataInStorage(this.storageCostKey);
        let category = await getDataInStorage(this.storageCategoryKey);

        this.setState({costs: costs || [], category: category || []})
      }

    updateDataCategoryInStorage = async (data) => {
        await setDataInStorage(this.storageCategoryKey, data);
        let category = await getDataInStorage(this.storageCategoryKey);

        this.setState({category})
    }

    updateDataCostInStorage = async (data) => {
        await setDataInStorage(this.storageCostKey, data);
        let costs = await getDataInStorage(this.storageCostKey);

        this.setState({costs: costs})
    }

    render() {
        let today = moment().set({'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0});

        return (
            <View style={{ height: "100%" }}>
                
                <Tabs uppercase={false}>
                    <TabScreen AppBar={{ color: 'red' }} label="День" style={{ fontSize: 10 }}>
                    <View>
                        <CostsPageTest 
                        offset={today.valueOf()} 
                        costs={this.state.costs} 
                        category={this.state.category} 
                        onChangeCosts={this.updateDataCostInStorage.bind(this)}
                        onChangeCategory={this.updateDataCategoryInStorage.bind(this)}></CostsPageTest>
                    </View>
                    </TabScreen>
                    <TabScreen label="Неделя">
                        <View>
                            <CostsPageTest 
                            offset={today.day(-6).valueOf()} 
                            costs={this.state.costs} 
                            category={this.state.category} 
                            onChangeCosts={this.updateDataCostInStorage.bind(this)}
                            onChangeCategory={this.updateDataCategoryInStorage.bind(this)}></CostsPageTest>
                        </View>
                    </TabScreen>
                    <TabScreen label="Месяц">
                        <View>
                            <CostsPageTest 
                            offset={today.date(1).valueOf()} 
                            costs={this.state.costs} 
                            category={this.state.category} 
                            onChangeCosts={this.updateDataCostInStorage.bind(this)}
                            onChangeCategory={this.updateDataCategoryInStorage.bind(this)}></CostsPageTest>
                        </View>
                    </TabScreen>
                    <TabScreen label="Год">
                        <View>
                            <CostsPageTest 
                                offset={today.month(0).valueOf()} 
                                costs={this.state.costs} 
                                category={this.state.category} 
                                onChangeCosts={this.updateDataCostInStorage.bind(this)}
                                onChangeCategory={this.updateDataCategoryInStorage.bind(this)}></CostsPageTest>
                        </View>
                    </TabScreen>
                </Tabs>
            </View>
        );
    }
}
