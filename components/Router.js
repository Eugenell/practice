import * as React from "react";
import { View } from "react-native";
import { BottomNavigation } from "react-native-paper";
import Header from "./Header";
import TabsCosts from './TabsCosts';
import TabsIncome from './TabsInсome'

const CostsRoute = () => (
    <View>
        <Header title="Журнал расходов"></Header>
        <TabsCosts></TabsCosts>
    </View>
);

const IncomeRoute = () => (
    <View>
        <Header title="Журнал доходов"></Header>
        <TabsIncome></TabsIncome>
    </View>
);

const Router = () => {
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: "costs", title: "Расходы", icon: "cart-plus" },
        { key: "income", title: "Доходы", icon: "wallet" },
    ]);

    const renderScene = BottomNavigation.SceneMap({
        costs: CostsRoute,
        income: IncomeRoute,
    });

    return <BottomNavigation navigationState={{ index, routes }} onIndexChange={setIndex} renderScene={renderScene} />;
};

export default Router;
