import React, { useState } from "react";
import { ScrollView, View, Picker, StyleSheet, Dimensions } from "react-native";
import { DataTable, FAB, Button, Paragraph, Dialog, Portal, TextInput } from "react-native-paper";
import { PieChart } from "react-native-chart-kit";

import { dataSalary } from "../../src/dataSalary";

export default function IncomePage() {
    let sum = 0;

    for (let i = 0; i < dataSalary.length; i++) {
        sum += dataSalary[i].money;
    }

    const [visible, setVisible] = useState(false);

    const showDialog = () => setVisible(true);

    const hideDialog = () => setVisible(false);

    const [selectedValue, setSelectedValue] = useState("car");

    return (
        //blue
        <ScrollView style={{ height: "77%" }}>
            <PieChart data={dataSalary} width={Dimensions.get("window").width} height={250} avoidFalseZero='true' chartConfig={{ color: (opacity = 1) => `rgba(255,255,255, ${opacity})` }} accessor={"money"} backgroundColor={"transparent"} center={[15, 0]} />
            <View style={{alignItems: 'center', paddingBottom: 20}}><FAB style={styles.fab} icon="plus" onPress={showDialog} label="добавить доход"/></View>
            <DataTable>
                <DataTable.Header>
                    <DataTable.Title>Категория</DataTable.Title>
                    <DataTable.Title numeric>Проценты</DataTable.Title>
                    <DataTable.Title numeric>Стоимость</DataTable.Title>
                </DataTable.Header>
                <DataTable.Row>
                    <DataTable.Cell>{dataSalary[0].name}</DataTable.Cell>
                    <DataTable.Cell numeric>{Math.round((dataSalary[0].money * 100) / sum)}%</DataTable.Cell>
                    <DataTable.Cell numeric>{dataSalary[0].money}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>{dataSalary[1].name}</DataTable.Cell>
                    <DataTable.Cell numeric>{Math.round((dataSalary[1].money * 100) / sum)}%</DataTable.Cell>
                    <DataTable.Cell numeric>{dataSalary[1].money}</DataTable.Cell>
                </DataTable.Row>
                <DataTable.Row>
                    <DataTable.Cell>Итого</DataTable.Cell>
                    <DataTable.Cell numeric>{sum}</DataTable.Cell>
                </DataTable.Row>
            </DataTable>
            <Portal>
                <Dialog visible={visible} onDismiss={hideDialog}>
                    <Dialog.Title>Добавить доход</Dialog.Title>
                    <Dialog.Content>
                        <Paragraph>Выберите категорию: </Paragraph>
                        <Picker selectedValue={selectedValue} style={{ height: 50, width: "100%" }} onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}>
                            <Picker.Item label="Оф.Заработок" value="car" />
                            <Picker.Item label="Хобби" value="food" />
                        </Picker>
                        <TextInput outlined label="Введите сумму" />
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button icon="plus" onPress={hideDialog}>
                            Добавить
                        </Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    fab: {
        alignItems: 'flex-end',
        bottom: 0,
        right: 0,
        color: "#fff",
        backgroundColor: "#6200EE",
        width: '60%',
        alignItems: 'center'
    },
});
