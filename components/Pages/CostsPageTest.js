import React, { useState, Component, Fragment } from "react";
import { ScrollView, View, Picker, StyleSheet, Dimensions } from "react-native";
import { DataTable, FAB, Paragraph, Dialog, Portal, TextInput, Button, Text } from "react-native-paper";

import { PieChart } from "react-native-chart-kit";

import DatePicker from 'react-native-datepicker';

import moment from 'moment';

export default class CostsPageTest extends Component {

    constructor(props) {
        super(props);

        this.state = {category: this.props.category, 
            costs: this.parseCosts(this.props.costs) || [], 
            selectedDate: new Date(), 
            categoryColorValue: '#A29BFE',
            isVisibleNewCategory: false };
      }

    async componentDidMount() {    
      
    }

    parseCosts(costs) {
        let currentCosts = costs && costs.filter(c => moment(c.date).valueOf() >= this.props.offset);
        let newArr = [];

        currentCosts && currentCosts.forEach(c => {
            let index = newArr && newArr.findIndex(x => x.name == c.name);
            if (index != null && index != -1) 
                newArr[index].money += c.money;
            else 
                newArr.push(Object.assign({}, c));

        })

        return newArr;
    }

     showDialog = () => this.setState({visible:true});

     hideDialog = async () => {
        let costs = this.props.costs || [];

        if (this.state.selectedValue && this.state.categoryMoney) {
            let category = this.props.category.find(c => c.name == this.state.selectedValue);
            if (category)
            costs.push({name: this.state.selectedValue, 
                money: parseInt(this.state.categoryMoney), 
                color: category.color || 'black', 
                legendFontColor: "#7F7F7F", legendFontSize: 15, 
                date: this.state.selectedDate});

            this.props.onChangeCosts(costs);
        }

        this.setState({visible:false});
    };

    render() {
        let costs = this.parseCosts(this.props.costs);
        let sum = 0;
        costs.forEach(c => sum+=parseFloat(c.money));

        let isVisibleNewCategory = this.state.isVisibleNewCategory || !(this.props.category && this.props.category.length != 0);

        return (
        <ScrollView style={{ height: "77%" }}>
            {costs && costs.length > 0 &&
                <PieChart 
                    data={costs}
                    width={Dimensions.get('window').width}
                    height={250}
                    chartConfig={
                        {color: (opacity = 1) => `rgba(255,255,255, ${opacity})`,}
                    }
                    accessor={'money'}
                    backgroundColor={"transparent"}
                    paddingLeft={"15"}
                    center={[15, 0]}
                    absolute
                />
            }
            
            <View style={{alignItems: 'center', paddingBottom: 20}}>
                <FAB style={styles.fab} icon="plus" onPress={this.showDialog} label="добавить расход"/>
                
            </View>

            <DataTable>
                <DataTable.Header>
                    <DataTable.Title>Категория</DataTable.Title>
                    <DataTable.Title numeric>Проценты</DataTable.Title>
                    <DataTable.Title numeric>Стоимость</DataTable.Title>
                </DataTable.Header>
                {costs.map((data,index) => 
                    (<DataTable.Row key={index}>
                        <DataTable.Cell>{data.name}</DataTable.Cell>
                        <DataTable.Cell numeric>{Math.round((data.money * 100) / sum)}%</DataTable.Cell>
                        <DataTable.Cell numeric>{data.money}</DataTable.Cell>
                    </DataTable.Row>)
                )}
            </DataTable>
            <Portal>
                <Dialog visible={this.state.visible} onDismiss={this.hideDialog}>
                    <Dialog.Title>Добавить расход</Dialog.Title>
                    <Dialog.Content>
                        {!isVisibleNewCategory &&
                        (<Fragment>
                            <Paragraph>Выберите категорию: </Paragraph>
                            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                                <Picker selectedValue={this.state.selectedValue || null}
                                    style={{ height: 50, width: "80%" }} 
                                    onValueChange={(itemValue, itemIndex) => this.setState({showDatePicker: true,selectedValue: itemValue})}>

                                    {this.props.category.map((data, index) => 
                                        <Picker.Item key={index} label={data.name} value={data.name} />
                                    )}
                                </Picker>
                                
                                <FAB small style={{ alignItems: 'center', height: 40, width: '20%', color: '#fff', backgroundColor: "#6200EE"}} icon="plus" onPress={() => this.setState({isVisibleNewCategory: true})} />
                            </View>

                            <TextInput outlined label="Введите сумму" 
                                onChangeText={text => this.state.categoryMoney = text}/>

                        <DatePicker
                            style={{width: '100%'}}
                            date={this.state.selectedDate}
                            mode="date"
                            placeholder="select date"
                            format="YYYY-MM-DD"
                            maxDate={new Date()}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                            }}
                            onDateChange={(date) => {this.setState({selectedDate: date})}}
                        />
                        </Fragment>) }

                        {isVisibleNewCategory &&
                            (<Fragment>
                                <TextInput label="Добавить категорию" 
                                    onChangeText={text => this.state.categoryText = text}/>

                                <Picker  selectedValue={this.state.categoryColorValue}
                                    style={{ height: 50, width: "100%" }} 
                                    onValueChange={(itemValue, itemIndex) => this.setState({categoryColorValue: itemValue})}
                                    >
                                    
                                    {['#A29BFE', '#FF7675', '#FDCB6E', '#74B9FF', '#00B894', '#6800b8', '#b8004d', '#96b800'].map((color, index) => 
                                        <Picker.Item key={index} color={color} value={color} label={color} />
                                    )}
                                </Picker>

                                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                                    <FAB style={styles.fabSmall} small icon="plus" onPress={() => {
                                        let category = this.props.category || [];

                                        category.push({name: this.state.categoryText, color: this.state.categoryColorValue});

                                        this.props.onChangeCategory(category)
                                        this.setState({isVisibleNewCategory: false})}} />
                                        
                                    <FAB style={styles.fabSmall} small icon="cancel" onPress={() => {this.setState({isVisibleNewCategory: false})}} />
                                </View>
                            </Fragment>) }
                    </Dialog.Content>
                    <Dialog.Actions>
                    {!isVisibleNewCategory &&
                        <Button icon="plus" onPress={this.hideDialog}>
                            Добавить
                        </Button>}
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </ScrollView>
    );
    }
}

const styles = StyleSheet.create({
    fab: {
        alignItems: 'flex-end',
        bottom: 0,
        right: 0,
        color: "#fff",
        backgroundColor: "#6200EE",
        width: '60%',
        alignItems: 'center'
    },

    fabSmall: {
        alignItems: 'flex-end',
        bottom: 0,
        right: 0,
        color: "#fff",
        backgroundColor: "#6200EE",
        width: '50%',
        alignItems: 'center'
    }
});
