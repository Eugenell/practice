import React, { useState } from "react";
import { ScrollView, View, Text, Picker, StyleSheet, Dimensions } from "react-native";
import { DataTable, FAB, Button, Paragraph, Dialog, Portal, TextInput } from "react-native-paper";
import { PieChart } from "react-native-chart-kit";

import { data } from "../../src/data";

export default function CostsPage() {
    let sum = 0;

    for (let i = 0; i < data.length; i++) {
        sum += data[i].money;
    }

    const [visible, setVisible] = useState(false);

    const showDialog = () => setVisible(true);

    const hideDialog = () => setVisible(false);

    const [selectedValue, setSelectedValue] = useState("car");

    return (
        //blue
        <ScrollView style={{ height: "77%" }}>
            <PieChart data={data} width={Dimensions.get("window").width} height={250} chartConfig={{ color: (opacity = 1) => `rgba(255,255,255, ${opacity})` }} accessor={"money"} backgroundColor={"transparent"} center={[15, 0]} />
            <View style={{alignItems: 'center', paddingBottom: 20}}><FAB style={styles.fab} icon="plus" onPress={showDialog} label="добавить расход"/></View>
            <DataTable>
                <DataTable.Header>
                    <DataTable.Title>Категория</DataTable.Title>
                    <DataTable.Title numeric>Проценты</DataTable.Title>
                    <DataTable.Title numeric>Стоимость</DataTable.Title>
                </DataTable.Header>
                <DataTable.Row>
                    <DataTable.Cell>{data[0].name}</DataTable.Cell>
                    <DataTable.Cell numeric>{Math.round((data[0].money * 100) / sum)}%</DataTable.Cell>
                    <DataTable.Cell numeric>{data[0].money}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>{data[1].name}</DataTable.Cell>
                    <DataTable.Cell numeric>{Math.round((data[1].money * 100) / sum)}%</DataTable.Cell>
                    <DataTable.Cell numeric>{data[1].money}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>{data[2].name}</DataTable.Cell>
                    <DataTable.Cell numeric>{Math.round((data[2].money * 100) / sum)}%</DataTable.Cell>
                    <DataTable.Cell numeric>{data[2].money}</DataTable.Cell>
                </DataTable.Row>
                <DataTable.Row>
                    <DataTable.Cell>{data[3].name}</DataTable.Cell>
                    <DataTable.Cell numeric>{Math.round((data[3].money * 100) / sum)}%</DataTable.Cell>
                    <DataTable.Cell numeric>{data[3].money}</DataTable.Cell>
                </DataTable.Row>
                <DataTable.Row>
                    <DataTable.Cell>{data[4].name}</DataTable.Cell>
                    <DataTable.Cell numeric>{Math.round((data[4].money * 100) / sum)}%</DataTable.Cell>
                    <DataTable.Cell numeric>{data[4].money}</DataTable.Cell>
                </DataTable.Row>
                <DataTable.Row>
                    <DataTable.Cell>Итого</DataTable.Cell>
                    <DataTable.Cell numeric>{sum}</DataTable.Cell>
                </DataTable.Row>
            </DataTable>
            <Portal>
                <Dialog visible={visible} onDismiss={hideDialog}>
                    <Dialog.Title>Добавить расход</Dialog.Title>
                    <Dialog.Content>
                        <Paragraph>Выберите категорию: </Paragraph>
                        <Picker selectedValue={selectedValue} style={{ height: 50, width: "100%" }} onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}>
                            <Picker.Item label="Автомобиль" value="car" />
                            <Picker.Item label="Еда и напитки" value="food" />
                            <Picker.Item label="Одежда" value="clothes" />
                            <Picker.Item label="Развлечения" value="entertainment" />
                            <Picker.Item label="Спорт" value="sport" />
                        </Picker>
                        <TextInput outlined label="Введите сумму" />
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button icon="plus" onPress={hideDialog}>
                            Добавить
                        </Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    fab: {
        alignItems: 'flex-end',
        bottom: 0,
        right: 0,
        color: "#fff",
        backgroundColor: "#6200EE",
        width: '60%',
        alignItems: 'center'
    },
});
