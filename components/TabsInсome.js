import React, { Component } from "react";
import { View } from "react-native";
import { Tabs, TabScreen } from "react-native-paper-tabs";
import IncomePage from './Pages/IncomePage';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class TabsIncome extends Component {
    render() {
        return (
            <View style={{ height: "100%" }}>
                <Tabs uppercase={false}>
                    <TabScreen AppBar={{ color: 'red' }} label="День" style={{ fontSize: 10 }}>
                        <View>
                            <IncomePage></IncomePage>
                        </View>
                    </TabScreen>
                    <TabScreen label="Неделя">
                        <View>
                            <IncomePage></IncomePage>
                        </View>
                    </TabScreen>
                    <TabScreen label="Месяц">
                        <View>
                            <IncomePage></IncomePage>
                        </View>
                    </TabScreen>
                    <TabScreen label="Год">
                        <View>
                            <IncomePage></IncomePage>
                        </View>
                    </TabScreen>
                </Tabs>
            </View>
        );
    }
}
