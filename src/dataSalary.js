export const dataSalary = [
    {
        key: 1,
        name: "Оф. заработок",
        money: 600,
        color: "#74B9FF",
        legendFontColor: "#7F7F7F",
        legendFontSize: 12,
    },
    {
        key: 2,
        name: "Хобби",
        money: 300,
        color: "#FDCB6E",
        legendFontColor: "#7F7F7F",
        legendFontSize: 12,
    },
];
