import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function setDataInStorage(storageKey, dataArray) {
    try {
        const jsonValue = JSON.stringify(dataArray);

        await AsyncStorage.setItem(`@${storageKey}`, jsonValue)
      } catch (e) {
        // saving error
      }
}

export async function clearStorage() {
  try {
    await AsyncStorage.clear();
  }
  catch(e) {
    //clear error
  }
}
