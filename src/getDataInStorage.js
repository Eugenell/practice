import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function getDataInStorage(storageKey) {
    try {
        const jsonValue = await AsyncStorage.getItem(`@${storageKey}`)
        return jsonValue != null ? JSON.parse(jsonValue) : null;
      } catch(e) {
        // error reading value
      }
}
